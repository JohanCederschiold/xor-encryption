package encryption;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class InputOutput {
	
	String filepath;
	String filename;
	
	
	public InputOutput(String filepath, String filename) {
	
		this.filepath = filepath;
		this.filename = filename;
	}
	
	
	public String readFile () {
		
		String text = "";
		Scanner scanner = null;
		
		try {
			scanner = new Scanner(new File(filepath));
			
			while (scanner.hasNextLine()) {
				text += scanner.nextLine();
			}
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			text = "File not found";
		} finally {
			scanner.close();
		}

		return text;
	}
	
	public void saveFile (String textToSave) {
		
		PrintWriter writer = null;
		
		try {
			writer = new PrintWriter(new BufferedWriter(new FileWriter(filepath)));
			writer.append(textToSave);
			
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			writer.close();
		}
		
	}
	
	

}
