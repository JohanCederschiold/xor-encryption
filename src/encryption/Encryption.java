package encryption;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Encryption {
	
//	Instansvariabler
	private String key; 
	
//	Konstruktor
	public Encryption (String key) {
		
		this.key = key;
	}
	
	public List<String> toggleEncryption (List<String> binarySentence) {
		
		/*	Metoden tar en array och "togglar" krypteringen p� den
		 *  ArrayListen utg�r hela texten och varje element �r ett ord. 
		 */
		
		List<String>encryptedSentence = new ArrayList<>();
		
		for (String binaryWord : binarySentence) {
			String encryptedWord = "";
			Scanner dividedLetters = new Scanner(binaryWord).useDelimiter(" ");
			while (dividedLetters.hasNext()) {
				encryptedWord += " " + convertLetter(dividedLetters.next());
				
			}
			dividedLetters.close();
			encryptedSentence.add(encryptedWord.trim());
		}
		return encryptedSentence;
	}
	
	public boolean setKey (String binaryKey) {
		
//		Kallar en statisk metod i BinaryToggler som s�tter nyckeln till "r�tt" l�ngd.
		String key = BinaryToggler.correctLength(binaryKey);
		
		System.out.println(key);
		
		
		for (int i = 0 ; i < key.length() ; i++ ) {
//			Kontrollera att str�ngen endast inneh�ller till�tna tecken. 
			if(key.charAt(i) != '0' && key.charAt(i) != '1') {
				System.out.println(key.charAt(i));
				return false;
			} 
		}
		
		this.key = key;
		return true; //Allt ok nyckeln �r satt.
		
		
		
	}
	
	private String convertLetter (String binaryLetter) {
//		Metoden crypterar en bin�r bokstav baserat p� nyckeln
		
		String convertedBinaryLetter = "";
		
		if (binaryLetter.length() != key.length()) {
			return null;
		}
		
		/* Loopen kontrollerar och j�mf�r siffrorna p� varje indexplats i den bin�ra 
		 * bokstaven med nyckeln. 
		 * Enlig XOR-principen blir tv� lika siffror = 0 och tv� olika = 1.
		 * https://en.wikipedia.org/wiki/XOR_cipher
		 * 
		 */
		for (int i = 0; i < key.length() ; i++ ) {
			if (binaryLetter.charAt(i) == key.charAt(i)) {
				convertedBinaryLetter += "0";
			} else {
				convertedBinaryLetter += "1";
			}
		}
		
		return convertedBinaryLetter;
	}

}
