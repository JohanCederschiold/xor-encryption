package encryption;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class BinaryToggler {
	
	private static final int binaryLength = 10;
	
	public List<String> convertToBinary (String sentence) {
		
//		Skapar en lista att lagra den bin�ra meningen i (ett element = ett ord).
		List<String>binarySentence = new ArrayList<>();
		
//		En delimiter delar upp orden. 
		Scanner splitIntoWords = new Scanner(sentence).useDelimiter(" ");
		
//		Konverterar till bin�ra ord och l�gger till.
		while (splitIntoWords.hasNext()) {
			binarySentence.add(convertWordToBinary(splitIntoWords.next()));
		}

		return binarySentence;
	}
	
	public String convertFromBinary ( List<String> binarySentence ) {
		
//		Skapar str�ng f�r lagring av meningen
		String alphaNumericSentence = "";
		
//		Konverterar ord f�r f�r ord fr�n bin�rt till alfanumeriskt. 
		for (String binaryWord : binarySentence) {
			alphaNumericSentence += convertFromBinaryWord(binaryWord) + " ";
		}
		
//		Returnera ordet.
		return alphaNumericSentence.trim();
	}
   	
	
	
	private String convertWordToBinary (String word) {
		
		String binaryWord = "";
		char [] letterForLetter = word.toCharArray(); 
		
		/*	For loopen konverterar varje char till integer v�rde f�r att sedan konvertera integern till en Str�ng och,
		 *  innan den l�ggs till i String s� skickas den till metoden f�r att korrigera l�ngden (8 siffror).
		 */
		
		for (int i = 0; i < letterForLetter.length ; i++ ) {
			int letterNo = letterForLetter[i];
			binaryWord += " " + correctLength(Integer.toBinaryString(letterNo)); 
		}
		return binaryWord.trim();
	}
	
	private String convertFromBinaryWord (String binaryString) {
		
		Scanner splitBinaryString = new Scanner(binaryString).useDelimiter(" ");
		String convertedWord = "";
		
		/*	While loopen anv�nder delimitern f�r att dela upp bokst�verna i ordet och konvertera dem en och en. 
		 * 	Str�ngen konverteras till en decimal int, sedan till en char och l�ggs till str�ngen. 
		 */
		
		while (splitBinaryString.hasNext()) {
			char letter = (char) Integer.parseInt(splitBinaryString.next(), 2);
			convertedWord += letter;
		}
		splitBinaryString.close();
		return convertedWord;
	}
	
	static String correctLength (String binaryNumber) {
		/*	Metoden �r static f�r att �ven vara n�bar utanf�r klassen (utan instasiering).
		 * 	Metoden justerar l�ngden (enligt instansvariabel "binaryLength") genom
		 *  att fylla ut med nollor fr�mst i str�ngen. 
		 */
		
		String zerosAdded ="";
		
		for (int i = 0 ; i < binaryLength - binaryNumber.length() ; i++) {
			zerosAdded += 0;
		}
		zerosAdded += binaryNumber;
		return zerosAdded;
	}
	

}
