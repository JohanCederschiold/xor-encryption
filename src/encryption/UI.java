package encryption;

import java.awt.BorderLayout;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextArea;

public class UI extends JFrame {
	
//	Instansvariabler
	JTextArea txtArea = new JTextArea(10,20); 
	JPanel keyPanel = new JPanel();
	JPanel controlPanel = new JPanel();
	JLabel lblKey = new JLabel("Key:");
	JPasswordField pswField = new JPasswordField(20);		//Nyckelf�lt
	JButton btnConvert = new JButton("Konvertera");	
	JButton btnOpen = new JButton("�ppna");	
	JButton btnSave = new JButton("Spara");	
	String [] printOptions = {"Sk�rm", "Fil" };				//Options f�r JComboBox
 	JComboBox<String> choosePrintMethod = new JComboBox<>(printOptions);
 	JFileChooser fileChooser = new JFileChooser(System.getProperty("home.dir"));
	BinaryToggler toggler;
	Encryption crypto;
	
	
	public UI () {
		
		setTitle("Kryptering");
		setLayout(new BorderLayout());
		
//		Instansiera st�dklasserna
		toggler = new BinaryToggler();
		crypto = new Encryption("00100100"); //Instansierar cryptoklassen med en default nyckel. 
		
//		L�gg till komponenter
		add(txtArea, BorderLayout.CENTER);
		txtArea.setLineWrap(true);
		txtArea.setWrapStyleWord(true);
		add(keyPanel, BorderLayout.NORTH);
		keyPanel.add(lblKey);
		keyPanel.add(pswField);
		add(controlPanel, BorderLayout.SOUTH);
		controlPanel.add(btnConvert);
		controlPanel.add(choosePrintMethod);
		controlPanel.add(btnOpen); btnOpen.setEnabled(false);
		controlPanel.add(btnSave); btnSave.setEnabled(false);
		txtArea.setEditable(true);
		
//		Add actionlistener
		btnConvert.addActionListener(l -> cryptoText());
		pswField.addActionListener(l -> changeKey());
		choosePrintMethod.addActionListener(l -> changePrintMethod());
		btnSave.addActionListener(l -> openSaveWindow());
		btnOpen.addActionListener(l -> openOpenWindow());
				
		pack();
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	private void cryptoText () {
		
//		Tar texten fr�n txtarea konverterar och skriver ut den.

		List<String> binaryText = toggler.convertToBinary(txtArea.getText());
		txtArea.setText("");
		List<String> convertedText = crypto.toggleEncryption(binaryText);
		String cryptoText = toggler.convertFromBinary(convertedText);
		
		txtArea.setText(cryptoText);
		
	}
	
	private void changeKey () {
		
//		H�mta nyckeln fr�n passwordf�ltet (char[]) och konvertera till str�ng
		String keyString = String.valueOf(pswField.getPassword());

//		F� bekr�ftelse p� att nyckeln faktiskt �r �ndrad
		boolean isSet = crypto.setKey(keyString);
		
//		Om �ndringen inte lyckades -> Radera texten fr�n passwordf�ltet. 
		if (isSet == false) {
			pswField.setText("");
		}
	}
	
	private void changePrintMethod () {
		
//		Metoden togglar �ppna / spara knapparna beroende p� anv�ndarens val
		
		int choice = choosePrintMethod.getSelectedIndex();
		
		if ( choice == 0 ) {
			btnOpen.setEnabled(false);
			btnSave.setEnabled(false);
		} else {
			btnOpen.setEnabled(true);
			btnSave.setEnabled(true);
		}
	}
	
	public void openSaveWindow () {
		
//		�ppnar en dialogruta d�r anv�ndaren f�r v�lja namnet och platsen f�r filen som skall sparas. 
				
		int choice = fileChooser.showSaveDialog(null);
		
		if (choice == fileChooser.APPROVE_OPTION) {
			
			InputOutput io = new InputOutput(fileChooser.getSelectedFile().getPath(), 
					fileChooser.getSelectedFile().getName());
			
			io.saveFile(txtArea.getText());
		}	
	}
	
	public void openOpenWindow () {
		
//		�ppnar en dialogruta d�r anv�ndaren f�r v�lja vilken fil som skall �ppnas. 
		
		int choice = fileChooser.showOpenDialog(null);
		
		if (choice == fileChooser.APPROVE_OPTION) {

			InputOutput io = new InputOutput(fileChooser.getSelectedFile().getPath(), 
					fileChooser.getSelectedFile().getName());
			
			txtArea.setText(io.readFile());

		}
	}
	
	
	public static void main(String[] args) {
		new UI();
	}

}
